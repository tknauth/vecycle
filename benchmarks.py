#!/usr/bin/python2.7 -W all

import pdb
import time
import subprocess as sp
from shlex import split
import sys
import numpy as np
import itertools
import os
import os.path
import multiprocessing
from collections import defaultdict
from xml.dom.minidom import parse
import random
import itertools

base_ip = 122

vars = {'migration' : ['vanilla', 'custom'],
        'network' : {'lan', 'wan'},
        'memory_mb' : [4*1024],
        'disk' : ['hdd', 'ssd'],
        'update' : [0, 25, 50, 75, 100]}

# vars = {'migration' : ['vanilla', 'custom'],
#         'network' : {'lan', 'wan'},
#         'memory_mb' : [6*1024],
#         'disk' : ['hdd', 'ssd']}

key_order = ['migration', 'network', 'memory_mb', 'disk', 'update']

script_start_timestamp = time.strftime("%Y%m%dT%H%M%S")

def wait_for_http_server(ip, port=80, url_path='') :
    count = 0
    url = "http://%s:%d/%s"%(ip,port,url_path)
    while 0 != sp.call(split("curl --fail %s"%(url)), stdout=None) :
        count += 1
        time.sleep(1)
        if count >= 40 :
            print 'failing ... web service did not come up'
            sys.exit(1)

def try_tcp_connect(ip, port) :
    rc = sp.call(split("nc -w 1 -z %s %s"%(ip, str(port))))
    return rc

def wait_till_shutdown(virsh_name) :
    while True :
        rc = sp.call("virsh list | grep %s"%(virsh_name),
                     stdout=None, stderr=None, shell=True)
        if rc != 0 :
            break
        time.sleep(1)

def setup_network_wan() :
    # Bandwidth and latency values taken from CloudNet (VEE 2011) paper:
    # Average latency 27 msec and a max throughput of 465 Mbps.

    for (src_ip, dst_ip) in itertools.permutations(['192.168.2.2', '192.168.2.3']) :
        sp.check_call(split('ssh %s sudo tc qdisc add dev br0 root handle 1: prio'%(src_ip)))
        sp.check_call(split('ssh %s sudo tc qdisc add dev br0 parent 1:3 handle 30: tbf rate 500mbit buffer 1600 limit  3000'%(src_ip)))
        sp.check_call(split('ssh %s sudo tc qdisc add dev br0 parent 30:1 handle 31: netem  delay 27ms 3ms distribution normal'%(src_ip)))
        sp.check_call(split('ssh %s sudo tc filter add dev br0 protocol ip parent 1:0 prio 3 u32 match ip dst %s/32 flowid 1:3'%(src_ip, dst_ip)))

def tear_network_wan() :
    for ip in ['192.168.2.2', '192.168.2.3'] :
        sp.call(split('ssh %s sudo tc qdisc del dev br0 root'%(ip)))

class Virsh() :
    def start(self, id, p) :
        return 'virsh start %s'%(id)
    def restore(self, id, p) :
        return 'virsh restore /mnt/%s.virsh'%(id)
    def suspend(self, id, p) :
        return 'virsh save %s /mnt/%s.virsh'%(id, id)
    def stop(self, id, p) :
        return 'virsh shutdown %s'%(id)
    def dumpws(self, id, p) :
        return 'virsh qemu-monitor-command --hmp %s "dumpws %s"'%(virsh_name, working_set_type)

class Qemu() :

    start_cmd = 'sudo /home/thomas/phd/veecycle/qemu/x86_64-softmmu/qemu-system-x86_64 -enable-kvm -M pc-1.0 -m %d -smp 1,sockets=1,cores=1,threads=1 \
-nographic -nodefconfig -nodefaults \
-rtc base=utc -no-shutdown -boot c -drive file=/mnt/nfs/vm0.qcow2,cache=none,format=qcow2 \
-monitor telnet:localhost:7001,server,nowait,nodelay \
-net nic,macaddr=52:54:00:37:4d:01,model=virtio -net tap,script=/etc/qemu-ifup,downscript=/etc/qemu-ifdown \
-serial mon:telnet::8888,server,nowait'

    def __init__(self, memory_mb) :
        self.memory_mb = memory_mb
        self.start_cmd = self.start_cmd%(self.memory_mb)

    def start(self) :
        return self.start_cmd

    def restore(self, id, p) :
        assert id == '192.168.2.122'
        # disk_path = '/mnt/%s'%(appliance2image_name[p['appliance']])
        disk_path = '/mnt/vm.qcow2'
        cmd = self.start_cmd%(p['ramcfg_mb'], id, disk_path, self.mac)
        cmd += ' -incoming "exec: cat /mnt/%s.virsh"'%(id)
        if p['qemu'] in ['lazy', 'lazyws'] :
            cmd += ' -suspend-image /mnt/%s-suspend-image'%(id)

        return cmd

    def cont(self) :
        return 'echo cont | nc 127.0.0.1 7001'

    def suspend(self, id, p) :
        suspend_file = '/mnt/%s.virsh'%(id)
        cmd = '''echo "stop\n\
migrate_set_speed 1g\n\
migrate \\"exec: cat > %s\\"\n\
quit" | nc 127.0.0.1 7001'''
        cmd = cmd%(suspend_file)
        return cmd

    def migrate_incoming(self, checkpoint_path=None) :
        cmd = self.start_cmd + ' -incoming tcp:0:4444'
        if checkpoint_path :
            cmd += ' -checkpoint %s'%(checkpoint_path)
        cmd += ' -S' # Debug only. Prevents automatic start of VM.
        return cmd

    def migrate(self, dst_ip) :
        cmd = '''echo "migrate_set_speed 1g\n\
migrate \\"tcp:%s:4444\\"" | nc 127.0.0.1 7001'''
        cmd = cmd%(dst_ip)
        return cmd

    def stop(self) :
        cmd = '''echo "system_powerdown\n\
quit" | nc 127.0.0.1 7001'''
        return cmd

    def dump(self, filename) :
        return 'echo dump-pc-ram \\"%s\\" | nc 127.0.0.1 7001'%(filename)

    def quit(self) :
        return 'echo quit | nc 127.0.0.1 7001'

    def dumpws(self, id, p) :
        cmd = 'echo dumpws %s | nc 127.0.0.1 7001'%(p['ws'])
        return cmd

other_benchmarks = defaultdict(lambda : (lambda x,y : 0))

def try_tcp(virsh_name) :
    import socket
    try :
        s = socket.create_connection((virsh_name, 80), 0.1)
        s.close()
        return True
    except socket.error as e :
        print e
        return False

def run(v, virsh_name, time_to_response) :

    begin = time.time()
    if v['action'] == 'start' :
        p = sp.Popen(split(vmbackend.start(virsh_name, v)))
    elif v['action'] == 'resume' :
        p = sp.Popen(split(vmbackend.restore(virsh_name, v)))

    # time.sleep(0.1)
    while try_tcp(virsh_name) :
        pass

    while True :
        url_path = appliance2url_path[v['appliance']]
        rc = sp.call(split("curl --fail http://%s/%s"%(virsh_name, url_path)), stdout=None)
        if rc == 0 :
            break
    end = time.time()

    time_to_response.value = (end-begin)

#    other_benchmarks[v['appliance']](v, virsh_name)

def run_experiment(v, measurements) :
    pass

def get_report_filename() :
    script_basename = os.path.basename(__file__)
    return "%s-%s.csv"%(script_start_timestamp, script_basename)

def global_setup(virsh_name) :
    pass

def global_teardown(virsh_name) :
    pass

def get_rx_tx_bytes(ip) :
    stdout = sp.check_output('ssh %s grep br0 /proc/net/dev | head -n1'%(ip), shell=True)
    fields = stdout.split()
    rx_bytes, tx_bytes = fields[1], fields[9]
    return int(rx_bytes), int(tx_bytes)


class Experiment :

    def __init__(self, params) :
        self.params = params
        self.src_ip = '192.168.2.3'
        self.dst_ip = '192.168.2.2'
        self.vmbackend = Qemu(params['memory_mb'])

        if self.params['disk'] == 'hdd' :
            self.checkpoint_file = '/tmp/memimage'
        elif self.params['disk'] == 'ssd' :
            self.checkpoint_file = '/mnt/ssd/memimage'
        else :
            assert False

    def migrate(self) :

        cmd = None
        if self.params['migration'] == 'custom' :
            cmd = self.vmbackend.migrate_incoming(self.checkpoint_file)
        else :
            cmd = self.vmbackend.migrate_incoming()
        sp.check_call(['ssh', self.dst_ip, 'nohup %s >qemu.stdout 2> qemu.stderr </dev/null &'%(cmd)])

        time.sleep(60)
        begin = time.time()
# p = sp.Popen(['ssh', '192.168.2.2', '/usr/bin/time', '-f', '"%e"', 'bash -c "echo dump-pc-ram \\"/tmp/memimage\\" | nc 127.0.0.1 7001"'], stderr=sp.PIPE)
        cmd = "bash -c '%s'"%(self.vmbackend.migrate(self.dst_ip))
        print cmd
        p = sp.Popen(['ssh', self.src_ip, '/usr/bin/time', '-f', '"%e"', cmd],
                     stderr=sp.PIPE)
        (stdoutdata, stderrdata) = p.communicate()
        print stderrdata

        end = time.time()

        sp.check_call(['ssh', self.dst_ip, self.vmbackend.cont()])

        delta_seconds = end - begin

        return float(stderrdata.strip())

    def setup(self) :
        if self.params['migration'] == 'custom' :
            sp.check_call(split('ssh %s sudo rm -f %s'%(self.src_ip, self.checkpoint_file)))
            sp.check_call(split('ssh %s sudo rm -f %s'%(self.dst_ip, self.checkpoint_file)))

        if self.params['network'] == 'wan' :
            setup_network_wan()

    def tear(self) :
        if self.params['network'] == 'wan' :
            tear_network_wan()

    def wait_until_update_complete(self) :
        max_wait_sec = 5 * 60
        begin = time.time()
        while time.time() - begin < max_wait_sec :
            out = sp.check_output(split('curl -s http://192.168.2.101/status'))
            if out.strip() == 'done' :
                print >> sys.stderr, 'update took approx %.2f seconds'%(time.time() - begin)
                return
            time.sleep(5)

        # Operation did not complete in time.
        assert False


    def execute_single_parameter_combination(self, measures) :

        sp.check_call(['ssh', self.src_ip, 'nohup %s >qemu.stdout 2>qemu.stderr </dev/null &'%(self.vmbackend.start())])

        max_iterations = 30
        while try_tcp_connect('192.168.2.101', '22') and max_iterations > 0:
            time.sleep(1)
            max_iterations -= 1
        assert max_iterations >= 0

        time.sleep(5) # Let the VM settle down a little

        alloc_size_mb = self.params['memory_mb'] * 9 / 10
        sp.check_call(['curl', 'http://192.168.2.101/create/%d'%(alloc_size_mb)])
        sp.check_call(['curl', 'http://192.168.2.101/update/100'])

        self.wait_until_update_complete()

        max_iterations = 4
        if self.params['migration'] == 'vanilla' : max_iterations = 1

        while max_iterations > 0 :
            rx_bytes_before, tx_bytes_before = get_rx_tx_bytes(self.src_ip)

            sp.check_call(['curl', 'http://192.168.2.101/update/%d'%(self.params['update'])])
            self.wait_until_update_complete()

            migration_time_seconds = self.migrate()
            rx_bytes_after, tx_bytes_after = get_rx_tx_bytes(self.src_ip)

            sp.check_call(['ssh', self.src_ip, self.vmbackend.dump(self.checkpoint_file)])
            sp.check_call(['ssh', self.src_ip, self.vmbackend.quit()])

            self.src_ip, self.dst_ip = self.dst_ip, self.src_ip
            max_iterations -= 1
            time.sleep(15+random.randint(0, 15))

            rx_bytes = rx_bytes_after - rx_bytes_before
            tx_bytes = tx_bytes_after - tx_bytes_before

            measures.append({'src_ip' : self.src_ip, 'dst_ip' : self.dst_ip, 'rx_bytes' : rx_bytes,\
                                 'tx_bytes' : tx_bytes, 'migration_time_seconds' : migration_time_seconds})

        sp.check_call(['ssh', self.src_ip, self.vmbackend.stop()])


if __name__ == '__main__' :

    filename = '%s-benchmarks.csv'%(script_start_timestamp)
    f = open(filename, 'w')

    for (cfg_count, tuple) in enumerate(itertools.product(*[vars[k] for k in key_order])) :
        parameters = {}
        for i in range(len(tuple)) : parameters[key_order[i]] = tuple[i]

        measurements = []
        exp = Experiment(parameters)
        exp.setup()
        exp.execute_single_parameter_combination(measurements)
        exp.tear()

        for m in measurements :
            for k in key_order :
                print >> f, '%s=%s'%(k, str(parameters[k])),

            print >> f, \
                'src_ip=%s'%(m['src_ip']), \
                'dst_ip=%s'%(m['dst_ip']), \
                'rx_bytes=%d'%(m['rx_bytes']), \
                'tx_bytes=%d'%(m['tx_bytes']), \
                'migration_time_seconds=%.2f'%(m['migration_time_seconds'])
        f.flush()

    f.close()

    sp.check_call(split('rm -f last-benchmarks.csv'))
    sp.check_call(split('ln -s %s last-benchmarks.csv'%(filename)))
