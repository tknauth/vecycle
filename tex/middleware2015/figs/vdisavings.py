import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp

# width = 2.3 # 3 plots side-by-side
width = 3.2
# Only 0.8 of typical height to squeeze more content into that paper.
height = width/1.3 * 0.8
fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'Times'
matplotlib.rcParams['text.usetex'] = True


import matplotlib.pyplot as plt

method2color={'dedup' : '#f1a340',
              'vecycle' : '#998ec3'}
method2marker={'dedup' : 'v',
               'vecycle' : 'o'}

traffic2color={'dedup' : '#8dd3c7',
               'vecycle' : '#bebada'}
traffic2marker={'dedup' : '^',
                'vecycle' : 's'}

nr_migrations = 25

def parse_input(data):
    data['dedup'] = np.array([0] * nr_migrations, float)
    data['vecycle'] = np.array([0] * nr_migrations, float)
    data['dirty_plus_dedup'] = np.array([0] * nr_migrations, float)

    with open('vdi-traffic-savings.txt') as f:
        for line in f:
            kv = {}
            recs = line.strip().split()
            for rec in recs:
                k, v = rec.split('=')
                kv[k] = v

            data['dedup'][int(kv['idx'])] = int(kv['deduplication'])
            data['dirty_plus_dedup'][int(kv['idx'])] = int(kv['dirty_plus_dedup'])
            data['vecycle'][int(kv['idx'])] = int(kv['hashes_plus_dedup'])

# xs may be a list of list. This creates a multi-plot figure.
def plot_vdi_savings() :

    # There is a script savings.py which outputs the values below.
    desktop_total_pages = 1539482
    data = {'native' : np.array([desktop_total_pages] * nr_migrations, float)}

    parse_input(data)
    
    fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(width, height))
    # ax1 = fig.add_subplot(111)

    xs = np.arange(len(data['native']))

    print len(data['vecycle'])
    barwidth = 0.5
    ax1.bar(xs, data['dedup']/data['native']*100.0, label='DEDUP',
            width = barwidth, linewidth=0.0,
            edgecolor='none', color=method2color['dedup'])
    ax1.set_title('Sender-side deduplication', fontsize=fontsize)

    ax2.bar(xs, data['vecycle']/data['native']*100.0, label='VeCycle',
            width = barwidth, linewidth=0.0,
            edgecolor='none', color=method2color['vecycle'])
    ax2.set_title('VeCycle', fontsize=fontsize, y=0.85) # move title down a bit

    # turn off ticks where there is no spine
    for ax in [ax1, ax2] :
        ax.set_yticks([0, 20, 40, 60, 80, 100])

        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
        ax.set_ylim(0)
        ax.set_xlim(0, nr_migrations)

        # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
        for loc, spine in ax.spines.iteritems():
            if loc in ['left','bottom']:
                pass
            elif loc in ['right','top']:
                spine.set_color('none') # don't draw spine
            else:
                raise ValueError('unknown spine location: %s'%loc)

    ax2.set_ylabel('Migration traffic [\% of RAM]', y=1.0)


    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])
    # ax1.set_yscale('log')

    plt.xlabel('Migration [\#]')
    # plt.ylabel('\% of full migration')


    # plt.legend(bbox_to_anchor=(0.5, 1.17), loc="upper center", numpoints=1,
    #            ncol=3)
    # leg = plt.gca().get_legend()
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # print dir(ltext)
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    print 'full %d pages'%(sum(data['native']))
    print 'dedup %d pages'%(sum(data['dedup']))
    print 'dirty_plus_dedup %d pages'%(sum(data['dirty_plus_dedup']))
    print 'VeCycle pages %d'%(sum(data['vecycle']))

    plt.savefig('vdisavings-%dx%d.pdf'%(width, height))

if __name__ == '__main__' :
    
    plot_vdi_savings()
