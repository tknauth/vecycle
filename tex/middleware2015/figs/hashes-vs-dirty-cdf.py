#! /usr/bin/env python2.7

import collections
import math
import matplotlib
matplotlib.use('pdf')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *
import sys
import os.path

fontsize = 7

pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
width = 2.3
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'Times'
matplotlib.rcParams['text.usetex'] = True

import matplotlib.pyplot as plt

# @return List of values to plot.
def parse_input(file):
    d = collections.defaultdict(float)

    for line in open(file, 'r') :
        kv_pairs = line.split()
        for (k, v) in map(lambda s : s.split('=', 1), kv_pairs):
            if k == 'dirty_tracking_and_deduplication':
                dirty = int(v)
            elif k == 'hashes_and_deduplication':
                hashes = int(v)
        
        improvement = 100. * (dirty - hashes) / float(dirty)
        d[improvement] += 1

    return d

def plot_cdf(file, linestyle, color, label) :

    d = parse_input(file)

    xs = sort(d.keys())
    ys1 = [d[x] for x in xs]
    ys1 = np.array(ys1)

    all = []
    for k in xs :
        all += [k]*int(d[k])
    # print int(sum(d.values())), len(all)
    assert len(all) == int(sum(d.values()))
    # print all[0:10], all[-11:-1]
    # for secs in [1.0, 10.0, 100.0] :
    #     print file, secs, stats.percentileofscore(all, secs, kind='weak')
    
    ys1 /= np.sum(ys1)
    # print 'ys1', ys1[0:10], ys1[-11:-1]
    # print 'xs', xs[0:10], xs[-11:-1]
    cys1 = np.cumsum(ys1)
    # print 'cys1', cys1[0:10], cys1[-11:-1]

    plt.plot(xs, cys1, linestyle, color=color, linewidth=1.0, clip_on=True, label=label)

# 3-class Dark2 www.colorbrewer2.org
name2color = {'00065BEE5AA7' : '#1b9e77',
              '00188B30D847' : '#d95f02',
              '001E4F36E2FB' : '#7570b3',
              '001B6333F86A' : '#1b9e77',
              '001B6333F90A' : '#d95f02',
              '001B6334DE9F' : '#7570b3',
              '001B6338238A' : '#998ec3'
              }

id2string = {'00065BEE5AA7' : 'Server A',
             '00188B30D847' : 'Server B',
             '001E4F36E2FB' : 'Server C',
             '001B6333F86A' : 'Laptop A',
             '001B6333F90A' : 'Laptop B',
             '001B6334DE9F' : 'Laptop C',
             '001B6338238A' : 'Laptop D'
             }

id2linestyle = {'00065BEE5AA7' : '-',
                '00188B30D847' : '-.',
                '001E4F36E2FB' : '--',
                '001B6333F86A' : '-',
                '001B6333F90A' : '-.',
                '001B6334DE9F' : '--',
                '001B6338238A' : '-'
             }

def plot_servers():
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    server_ids_small_to_larger = ['00065BEE5AA7', '00188B30D847', '001E4F36E2FB']
    for id in server_ids_small_to_larger:
        plot_cdf(os.path.expanduser('~/Dropbox/phd/veecycle/data/linux-servers/%s/compareMethods.csv' % (id)),
                 '-', name2color[id], id)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    ax1.set_ylim(0,1.02)

    # ax1.set_xscale('log')

    plt.xlabel('Reduction over dirty+dedup [\%]')
    plt.ylabel('CDF')
    # plt.legend(map(lambda id : id2string[id], server_ids_small_to_larger), loc=4)
    # leg = plt.gca().get_legend()
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # leg.draw_frame(False)

    plt.text(10, 0.86, 'Server A', rotation=45.0)
    plt.text(3, 1.01, 'Server B')
    plt.text(18, 0.6, 'Server C', rotation=50.0)

    plt.savefig('hashes-vs-dirty-cdf-servers-%din-%din'%(width, height))

def plot_laptops():

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    laptop_ids = ['001B6333F86A', '001B6333F90A', '001B6334DE9F', '001B6338238A']
    for id in laptop_ids:
        plot_cdf(os.path.expanduser('~/Dropbox/phd/veecycle/data/%s/compareMethods.csv' % (id)),
                 id2linestyle[id],
                 name2color[id],
                 id)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    ax1.set_ylim(0,1.02)

    # ax1.set_xscale('log')

    plt.xlabel('Reduction over dirty+dedup [\%]')
    plt.ylabel('CDF')
    # plt.legend(map(lambda id : id2string[id], laptop_ids), loc=4)
    handles, labels = ax1.get_legend_handles_labels()
    ax1.legend(handles[1:], map(lambda id : id2string[id], labels[1:]), loc=4)
    leg = plt.gca().get_legend()
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    leg.draw_frame(False)

    plt.text(10, 0.5, 'Laptop A', rotation=70.0)

    plt.savefig('hashes-vs-dirty-cdf-laptops-%din-%din'%(width, height))

def plot_generic(input_file):

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    plot_cdf(input_file, '-', '#000000')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    ax1.set_ylim(0,1.02)

    # ax1.set_xscale('log')

    plt.xlabel('Reduction over dirty tracking [\%]')
    plt.ylabel('CDF')
    # plt.legend(laptop_ids, loc=4)
    # leg = plt.gca().get_legend()
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # leg.draw_frame(False)

    plt.savefig('hashes-vs-dirty-cdf-%din-%din'%(width, height))

if __name__ == '__main__' :
    if len(sys.argv) < 2:
        plot_laptops()
        plot_servers()
    else :
        plot_generic(sys.argv[1])
