#! /usr/bin/env python2.7

import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import argparse
import os.path

fontsize = 7
width = 2.3
height = width/1.3

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("figure.subplot", left=(10/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(20/72.27)/height)
pylab.rc("figure.subplot", top=(height-10/72.27)/height)

matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'Times'
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :
    recs = []

    for line in open(filename) :
        line = line.strip()
        fields = line.split()
        kv = {}
        for field in fields :
            key, value = field.split('=')
            kv[key] = value
            
        recs.append(kv)
        
    return recs

method2color = {'deduplication' : '#f1a340',
                'dirty_tracking' : '#998ec3',
                'dirty_tracking_and_deduplication' : '#aa66aa',
                'hashes' : '#11aa99',
                'hashes_and_deduplication' : '#66aa55'
                }

# method2hatch = {'deduplication' : '',
#                 'dirty_tracking' : '+/',
#                 'dirty_tracking_and_deduplication' : '//',
#                 'hashes' : '\\/',
#                 'hashes_and_deduplication' : '..'
#                 }

methods = {'deduplication' : [],
           'dirty_tracking' : [],
           'dirty_tracking_and_deduplication' : [],
           'hashes' : [],
           'hashes_and_deduplication' : []
           }
baseline = 'total_pages'

method2label = {'deduplication' : 'dedup',
                'dirty_tracking' : 'dirty',
                'dirty_tracking_and_deduplication' : 'dirty+dedup',
                'hashes' : 'hashes',
                'hashes_and_deduplication' : 'hashes+dedup'}

def plot_single_server(file) :
    recs = parse_input(file)

    total_pages = int(recs[0][baseline])

    for rec in recs:
        for method in methods.keys():
            tx_pages = int(rec[method])
            methods[method].append(tx_pages / float(total_pages))
    
    bar_height = 1.0

    xs = 0.0
    inter_bar_gap = 1.0
    for method in methods.keys():
        print np.average(methods[method])
        ys = np.average(methods[method])
        plt.barh(xs, ys,
                 height=bar_height,
                 label=method2label[method],
                 color=method2color[method],
                 lw=0.5)

        plt.text(0.01, 0.5 * bar_height + xs, method2label[method],
                 rotation='horizontal',
                 va='center')

        plt.text(0.01 + ys, 0.5 * bar_height + xs, '%.2f' % (ys),
                 rotation='horizontal',
                 va='center')

        xs += inter_bar_gap

def set_axis_properties(ax1) :
    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    ax1.set_xticklabels([])
    ax1.set_yticks([])
    ax1.set_ylim(0, 5.5)
    ax1.set_xlim(0, 1)
    # ax1.set_yscale('log')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

def create_figure(args) :

    fig = plt.figure(figsize=(width, height))

    ax1 = fig.add_subplot(211)
    set_axis_properties(ax1)
    plot_single_server(os.path.expanduser('~/Dropbox/phd/veecycle/data/linux-servers/00065BEE5AA7/compareMethods.csv'))
    plt.title('Server A', fontsize=fontsize, y=0.9)

    # plt.legend(bbox_to_anchor=(0.45, 1.6), loc="upper center", numpoints=1,
    #            ncol=3, columnspacing=0.7,
    #            labelspacing=0.1,
    #            handlelength=1,
    #            handletextpad=0.2)
    # leg = plt.gca().get_legend()
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    ax1 = fig.add_subplot(212)
    set_axis_properties(ax1)
    plot_single_server(os.path.expanduser('~/Dropbox/phd/veecycle/data/linux-servers/00188B30D847/compareMethods.csv'))

    # plt.xlabel('Traffic reduction technique')
    plt.xlabel('Fraction of Baseline Traffic [0.0-1.0]')
    plt.title('Server B', fontsize=fontsize, y=0.9)
    # plt.title(args.title, fontsize=fontsize)

    plt.savefig('traffic-reduction-%din-%din.pdf'%(width, height))

def parse_arguments():
    parser = argparse.ArgumentParser(description='.')
    parser.add_argument('--title',
                        help='figure title')
    parser.add_argument('--inputfile',
                        help='File with data to plot.')
    args = parser.parse_args()
    return args

if __name__ == '__main__' :
    args = parse_arguments()
    create_figure(args)
