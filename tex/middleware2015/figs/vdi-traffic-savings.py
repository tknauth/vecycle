#!/usr/bin/env python2.7

import subprocess as sp
import datetime
import glob
import itertools
import os.path
import multiprocessing as mp
import collections
import sys
import re
import gzip
import Queue

sys.path.append('/mnt/myvg-data/home/thomas/Dropbox/phd/veecycle/code/')
import compareMethods

def get_datetime_from_filename(fn) :
    keyword = 'timestamp='
    idx = fn.find(keyword) + len(keyword)

    return datetime.datetime.strptime(fn[idx:idx+len('YYYY-mm-dd-HH-MM-SS')],
                                      '%Y-%m-%d-%H-%M-%S')


if __name__ == '__main__' :
    pattern = '~/Dropbox/phd/veecycle/data/desktop/*.gz'
    pattern = os.path.expanduser('~/Dropbox/phd/veecycle/data/desktop/*.gz')

    migration_times = [(datetime.time(8, 59), datetime.time(9, 01)),
                       (datetime.time(16,59), datetime.time(17, 01))]

    migrations = []
    for fn in glob.glob(pattern) :
        ts = get_datetime_from_filename(fn)
        
        if ts < datetime.datetime(2014, 11, 05) : continue
        if ts > datetime.datetime(2014, 11, 24) : continue

        # Ignore weekends
        if ts.date().isoweekday() in [6, 7] :
            continue

        for (l, u) in migration_times :
            lower = datetime.datetime.combine(ts.date(), l)
            upper = datetime.datetime.combine(ts.date(), u)
        
            if ts <= upper and ts >= lower :
                migrations.append(fn)

    migrations = sorted(migrations)
    for i in range(0, len(migrations)-1) :
        fp_new = compareMethods.Fingerprint(migrations[i])
        fp_old = compareMethods.Fingerprint(migrations[i-1])

        if i == 0 :
            print 'idx=%d' % (0), \
                'deduplication=%d' % (len(fp_old.hashes_set)), \
                'dirty_plus_dedup=%d' % (len(fp_old.hashes_set)), \
                'hashes=%d' % (len(fp_old.hashes_set)), \
                'hashes_plus_dedup=%d' % (len(fp_old.hashes_set))
            continue


        result = fp_new.calculate_migration_traffic(fp_old)
        print 'idx=%d'%(i), \
            'deduplication=%d' % (result.dedup), \
            'dirty_plus_dedup=%d' % (result.dirty_plus_dedup), \
            'hashes=%d' % (result.hashes), \
            'hashes_plus_dedup=%d' % (result.hashes_plus_dedup)
