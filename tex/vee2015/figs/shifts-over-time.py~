#!/usr/bin/env python2.7

import collections
import math
import argparse
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 2.2 # 3 plots side-by-side
# width = 6.6
height = 2.2/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename, max_time_delta_sec) :
    xys = []
    with open(filename) as f :
        for line in f.readlines() :
            line = line.strip()
            records = line.split()
            d = {}
            for (k, v) in map(lambda s : s.split('='), records) :
                d[k] = v

            time_delta_seconds = int(d['timedelta_sec'])

            if time_delta_seconds > max_time_delta_sec : continue

            y = collections.defaultdict(list)
            y['shifted_forward'] = int(d['shifted_forward']) / float(d['reusable_pages'])
            y['stationary'] = int(d['stationary']) / float(d['reusable_pages'])
            y['shifted_back'] = int(d['shifted_back']) / float(d['reusable_pages'])
            xy = (time_delta_seconds, y)
            xys.append(xy)
    return xys

def plot_single_figure(category) :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    max_time_delta_sec = 24 * 60 * 60
    xys = parse_input(sys.argv[1], max_time_delta_sec)

    # print xys
    xs = []
    ymin = []
    ymax = []
    yavg = []

    for upper_bound in range(30*60, max_time_delta_sec, 30*60) :
        lower_bound = upper_bound - 30*60
        bucket = filter(lambda (x,y) : x >= lower_bound and x < upper_bound, xys)
        if len(bucket) == 0 : continue
        xs.append(upper_bound)
        # print lower_bound, upper_bound, bucket
        ymin.append(min([y[category] for (x, y) in bucket]))
        ymax.append(max([y[category] for (x, y) in bucket]))
        yavg.append(np.average([y[category] for (x, y)  in bucket]))

    xs = [x/float(60*60) for x in xs]

    # percentiles = [25.0, 50.0, 90.0, 95.0, 99.0]
    # print percentiles, np.percentile(ys, percentiles)
    plt.plot(xs, ymin, linestyle='-', color='red', label='ymin', linewidth=1.0, clip_on=False)
    plt.plot(xs, ymax, linestyle='-', color='blue', label='ymax', linewidth=1.0, clip_on=False)
    plt.plot(xs, yavg, linestyle='-', color='green', label='yavg', linewidth=1.0, clip_on=False)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    # ax1.set_xticks([1,2,4,8,16,32])
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(0, 1)
    ax1.set_xlim(0)
    # ax1.set_yscale('log')

    plt.xlabel('Time between snapshots [hours]')
    plt.ylabel('Fraction of reusable pages [0.0-1.0]')

    title = {'stationary' : 'Stationary',
             'shifted_forward' : 'Shifted forward',
             'shifted_back' : 'Shifted back'}
    ax1.set_title(title[category], fontsize=fontsize)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # plt.legend(loc=2, numpoints=1, ncol=1)
    # leg = plt.gca().get_legend()
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    input_filename=os.path.basename(sys.argv[1])[0:-4]
    plot_dimensions='%din-%din'%(width, height)
    plt.savefig('%s-shifts-over-time-category=%s-%s'%(input_filename, category, plot_dimensions)+'.pdf')


if __name__ == '__main__' :
    plot_single_figure('stationary')
    plot_single_figure('shifted_forward')
    plot_single_figure('shifted_back')
