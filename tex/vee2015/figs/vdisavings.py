import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 2.3 # 3 plots side-by-side
width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

method2color={'cbr' : '#f1a340',
              'vecycle' : '#998ec3'}
method2marker={'cbr' : 'v',
               'vecycle' : 'o'}

traffic2color={'cbr' : '#8dd3c7',
               'vecycle' : '#bebada'}
traffic2marker={'cbr' : '^',
                'vecycle' : 's'}

# xs may be a list of list. This creates a multi-plot figure.
def plot_vdi_savings() :

    # There is a script savings.py which outputs the values below.
    data = {'native' : np.array([6144]*26, float),
            'cbr' : np.array([5513, 4452, 5484, 5644, 5616, 5616, 5261, 5374, 5202, 5101, 5065, 5002, 5004, 5190, 5195, 5368, 5373, 5507, 5380, 5237, 5282, 5302, 5302, 5510, 5534, 5537], float),
            'vecycle' : np.array([5513, 3246, 1908, 3347, 2125, 1370, 2358, 2744, 1305, 1169, 729, 1148, 389, 959, 411, 1066, 396, 2561, 605, 1182, 354, 1045, 353, 1030, 1590, 1255], float)}
    
    fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(width, height))
    # ax1 = fig.add_subplot(111)

    xs = np.arange(len(data['native']))

    print len(data['vecycle'])
    barwidth = 0.5
    ax1.bar(xs, data['cbr']/data['native']*100.0, label='CBR',
            width = barwidth, linewidth=0.0,
            edgecolor='none', color=method2color['cbr'])
    ax1.set_title('Sender-side deduplication')

    ax2.bar(xs, data['vecycle']/data['native']*100.0, label='VeCycle',
            width = barwidth, linewidth=0.0,
            edgecolor='none', color=method2color['vecycle'])
    ax2.set_title('VeCycle', y=0.85) # move title down a bit

    # turn off ticks where there is no spine
    for ax in [ax1, ax2] :
        ax.set_yticks([0, 20, 40, 60, 80, 100])

        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
        ax.set_ylim(0)
        ax.set_xlim(0, 26)

        # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
        for loc, spine in ax.spines.iteritems():
            if loc in ['left','bottom']:
                pass
            elif loc in ['right','top']:
                spine.set_color('none') # don't draw spine
            else:
                raise ValueError('unknown spine location: %s'%loc)

    ax2.set_ylabel('Migration traffic [\% of RAM]', y=1.0)


    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])
    # ax1.set_yscale('log')

    plt.xlabel('Migration [\#]')
    # plt.ylabel('\% of full migration')


    # plt.legend(bbox_to_anchor=(0.5, 1.17), loc="upper center", numpoints=1,
    #            ncol=3)
    # leg = plt.gca().get_legend()
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # print dir(ltext)
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    print 'full migration total %d MB'%(sum(data['native']))
    print 'CBR total %d MB'%(sum(data['cbr']))
    print 'VeCycle total %d MB'%(sum(data['vecycle']))

    plt.savefig('vdisavings-%dx%d.pdf'%(width, height))

if __name__ == '__main__' :
    
    plot_vdi_savings()
