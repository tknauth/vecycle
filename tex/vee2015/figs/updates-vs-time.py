import os
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 2.3
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :
    recs = []

    for line in open(filename) :
        line = line.strip()
        fields = line.split()
        kv = {}
        for field in fields :
            key, value = field.split('=')
            kv[key] = value
            
        recs.append(kv)
        
    return recs

method2color={'native' : '#f1a340',
              'custom' : '#998ec3'}
method2marker={'native' : 'v',
               'custom' : 'o'}

traffic2color={'native' : '#8dd3c7',
               'custom' : '#bebada'}
traffic2marker={'native' : '^',
                'custom' : 's'}

# xs may be a list of list. This creates a multi-plot figure.
def plot_migration_time(input_filename, network) :
    recs = parse_input(input_filename)
    
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    xs = sorted(list(set([int(r['update']) for r in recs])))
    xs = xs[1:]

    ys = []
    for x in xs :
        native_recs = filter(lambda r : r['migration'] == 'vanilla', recs)
        native_recs = filter(lambda r : r['disk'] == 'hdd', native_recs)
        native_recs = filter(lambda r : int(r['update']) == x, native_recs)
        native_recs = filter(lambda r : r['network'] == network, native_recs)
        y = np.average([float(r['migration_time_seconds']) for r in native_recs])
        print x, y
        ys.append(y)

    native = np.array(ys)
    plt.plot(xs, ys, color=method2color['native'],
             marker=method2marker['native'],
             label='QEMU 2.0', clip_on=False, linewidth=1.0)

    ys = []
    for x in xs :
        native_recs = filter(lambda r : r['migration'] == 'custom', recs)
        native_recs = filter(lambda r : r['disk'] == 'hdd', native_recs)
        native_recs = filter(lambda r : int(r['update']) == x, native_recs)
        native_recs = filter(lambda r : r['network'] == network, native_recs)
        values = [float(r['migration_time_seconds']) for r in native_recs]
        values = values[2:]
        y = np.average(values)
        print x, y
        ys.append(y)

    plt.plot(xs, ys, color=method2color['custom'],
             marker=method2marker['custom'],
             label='VeCycle', clip_on=False, linewidth=1.0)

    vecycle = np.array(ys)
    for x, y1, y2 in zip(xs[:-1], vecycle[:-1], native[:-1]) : # omit first value because too close together
        # print 'x=', x, y1, y2
        yoff = {'wan' : 50,
                'lan' : 3}
        plt.text(x, y1+yoff[network], '-%2.f\%%'%((1.-y1/y2)*100.), color='#EE3333', ha='center')

    # ys = []
    # for x in xs :
    #     native_recs = filter(lambda r : r['migration'] == 'vanilla', recs)
    #     native_recs = filter(lambda r : r['disk'] == 'ssd', native_recs)
    #     native_recs = filter(lambda r : int(r['memory_mb']) == x, native_recs)
    #     native_recs = filter(lambda r : r['network'] == network, native_recs)
    #     y = np.average([float(r['migration_time_seconds']) for r in native_recs])
    #     print x, y
    #     ys.append(y)

    # plt.plot(xs, ys, marker='x', label='native, ssd', clip_on=False)

    # ys = []
    # for x in xs :
    #     native_recs = filter(lambda r : r['migration'] == 'custom', recs)
    #     native_recs = filter(lambda r : r['disk'] == 'ssd', native_recs)
    #     native_recs = filter(lambda r : int(r['memory_mb']) == x, native_recs)
    #     native_recs = filter(lambda r : r['network'] == network, native_recs)
    #     values = [float(r['migration_time_seconds']) for r in native_recs]
    #     values = values[2:]
    #     y = np.average(values)
    #     print x, y
    #     ys.append(y)

    # plt.plot(xs, ys, marker='x', label='custom, ssd', clip_on=False)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(0)
    ax1.set_xlim(0)
    # ax1.set_yscale('log')

    plt.xlabel('Updates [\%]')
    plt.ylabel('Migration Time [seconds]')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(bbox_to_anchor=(0.5, 1.17), loc="upper center", numpoints=1, ncol=3)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('%s-updates-vs-time-network=%s-%dx%d.pdf'%(os.path.basename(input_filename).split('-')[0], network, width, height))

def plot_network_traffic(input_filename, network) :
    recs = parse_input(input_filename)
    
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    xs = sorted(list(set([int(r['update']) for r in recs])))
    xs = xs[1:]

    ys = []
    for x in xs :
        native_recs = filter(lambda r : r['migration'] == 'vanilla', recs)
        native_recs = filter(lambda r : r['disk'] == 'hdd', native_recs)
        native_recs = filter(lambda r : int(r['update']) == x, native_recs)
        native_recs = filter(lambda r : r['network'] == network, native_recs)
        y = np.average([float(r['tx_bytes']) for r in native_recs])
        print x, y
        ys.append(y)

    ys = map(lambda v : v/float(1024**3), ys)
    plt.plot(xs, ys, color=traffic2color['native'],
             marker=traffic2marker['native'],
             label='QEMU 2.0', clip_on=False, linewidth=1.0)

    ys = []
    for x in xs :
        native_recs = filter(lambda r : r['migration'] == 'custom', recs)
        native_recs = filter(lambda r : r['disk'] == 'hdd', native_recs)
        native_recs = filter(lambda r : int(r['update']) == x, native_recs)
        native_recs = filter(lambda r : r['network'] == network, native_recs)
        values = [float(r['tx_bytes']) for r in native_recs]
        values = values[2:]
        y = np.average(values)
        print x, y
        ys.append(y)

    ys = map(lambda v : v/float(1024**3), ys)
    plt.plot(xs, ys, color=traffic2color['custom'],
             marker=traffic2marker['custom'],
             label='VeCycle', clip_on=False, linewidth=1.0)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(0)
    ax1.set_xlim(0)
    # ax1.set_yscale('log')

    plt.xlabel('Updates [\%]')
    plt.ylabel('Tx traffic [GiB]')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(bbox_to_anchor=(0.5, 1.17), loc="upper center", numpoints=1, ncol=3)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('%s-updates-vs-traffic-network=%s-%dx%d.pdf'%(os.path.basename(input_filename).split('-')[0], network, width, height))

if __name__ == '__main__' :

    plot_migration_time(sys.argv[1], 'wan')
    plot_migration_time(sys.argv[1], 'lan')
    plot_network_traffic(sys.argv[1], 'lan')
