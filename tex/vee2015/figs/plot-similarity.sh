#!/usr/bin/env bash

python2.7 ~/Dropbox/phd/veecycle/age-vs-similarity-plot.py --data ~/Dropbox/phd/veecycle/data/linux-servers/00065BEE5AA7/00065BEE5AA7.csv --title 'Server A, Linux, 1 GiB' --annotate
python2.7 ~/Dropbox/phd/veecycle/age-vs-similarity-plot.py --data ~/Dropbox/phd/veecycle/data/linux-servers/00188B30D847/00188B30D847.csv --title 'Server B, Linux, 4 GiB'
python2.7 ~/Dropbox/phd/veecycle/age-vs-similarity-plot.py --data ~/Dropbox/phd/veecycle/data/linux-servers/001E4F36E2FB/001E4F36E2FB.csv --title 'Server C, Linux, 8 GiB'

python2.7 ~/Dropbox/phd/veecycle/age-vs-similarity-plot.py --data ~/Dropbox/phd/veecycle/data/001B6333F86A/001B6333F86A.csv --title 'Laptop A, OSX, 2 GiB'
python2.7 ~/Dropbox/phd/veecycle/age-vs-similarity-plot.py --data ~/Dropbox/phd/veecycle/data/001B6333F90A/001B6333F90A.csv --title 'Laptop B, OSX, 2 GiB'
python2.7 ~/Dropbox/phd/veecycle/age-vs-similarity-plot.py --data ~/Dropbox/phd/veecycle/data/001B6334DE9F/001B6334DE9F.csv --title 'Laptop C, OSX, 2 GiB'
