import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import os.path
import glob
import time
import datetime
fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 2.2 # 3 plots side-by-side
# width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(25/72.27)/width)
pylab.rc("figure.subplot", right=(width-1/72.27)/width)
pylab.rc("figure.subplot", bottom=(22/72.27)/height)
pylab.rc("figure.subplot", top=(height-18/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

id2label = {'00065BEE5AA7' : 'Server A',
            '00188B30D847' : 'Server B',
            '001E4F36E2FB' : 'Server C',
            '001B6333F86A' : 'Laptop A',
            '001B6333F90A' : 'Laptop B',
            '001B6334DE9F' : 'Laptop C'}
id2color = {'00065BEE5AA7' : '#3366aa',
            '00188B30D847' : '#11aa99',
            '001E4F36E2FB' : '#66aa55',
            '001B6333F86A' : '#3366aa',
            '001B6333F90A' : '#11aa99',
            '001B6334DE9F' : '#66aa55'}
id2marker = {'00065BEE5AA7' : 'x',
             '00188B30D847' : '+',
             '001E4F36E2FB' : '1',
             '001B6333F86A' : 'x',
             '001B6333F90A' : '+',
             '001B6334DE9F' : '1'}

def plot_zero_pages_over_time(input_directories) :
    
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    for dir in input_directories :
        xs = []
        ys = []

        with open(os.path.join(dir, 'self_similarity.csv')) as f :
            kv = {}
            for line in f :
                fields = line.strip().split()
                for field in fields :
                    k, v = field.split('=')
                    kv[k] = v

                time_hours = float(kv['timedelta_seconds'])/float(60*60)
                xs.append(time_hours)
                fraction_zero_pages = float(kv['zero_pages'])/float(kv['total_hashes'])
                ys.append(100.*fraction_zero_pages)

        server_id = dir.split('/')[-2]
        plt.plot(xs, ys, linestyle='',
                 marker=id2marker[server_id], markersize=4.0,
                 color=id2color[server_id],
                 label=id2label[server_id],
                 clip_on=False)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])

    ax1.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(24))
    ax1.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(12))

    ax1.set_ylim(0)
    ax1.set_xlim(0)
    # ax1.set_yscale('log')

    plt.xlabel('Time [hours]')
    plt.ylabel('Zero pages [\%]')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(bbox_to_anchor=(0.39, 1.3), loc="upper center", numpoints=1, ncol=3,
               columnspacing=0.4, handletextpad=0.1)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('zero-pages-over-time.pdf')

def plot_self_similarity_over_time(input_directories, tag) :
    
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    for dir in input_directories :
        xs = []
        ys = []
        with open(os.path.join(dir, 'self_similarity.csv')) as f :
            kv = {}
            for line in f :
                fields = line.strip().split()
                for field in fields :
                    k, v = field.split('=')
                    kv[k] = v

                time_hours = float(kv['timedelta_seconds'])/float(60*60)
                xs.append(time_hours)
                fraction_unique_hashes = float(kv['unique_hashes'])/float(kv['total_hashes'])
                fraction_duplicate_hashes = 1 - fraction_unique_hashes
                ys.append(100. * fraction_duplicate_hashes)

        server_id = dir.split('/')[-2]
        plt.plot(xs, ys, linestyle='',
                 marker=id2marker[server_id], markersize=4.0,
                 label=id2label[server_id],
                 color=id2color[server_id], clip_on=False)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks([24, 2*24, 3*24, 4*24, 5*24, 6*24, 7*24])

    # ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(0)
    ax1.set_xlim(0)
    # ax1.set_yscale('log')

    plt.xlabel('Time [hours]')
    plt.ylabel('Duplicate pages [\%]')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    ax1.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(24))
    ax1.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(12))

    plt.legend(bbox_to_anchor=(0.39, 1.3), loc="upper center", numpoints=1, ncol=3,
               columnspacing=0.4, handletextpad=0.1)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('self-similarity-over-time-%s.pdf'%(tag))

if __name__ == '__main__' :
    input_directories=['~/Dropbox/phd/veecycle/data/linux-servers/00065BEE5AA7/',
                       '~/Dropbox/phd/veecycle/data/linux-servers/00188B30D847/',
                       '~/Dropbox/phd/veecycle/data/linux-servers/001E4F36E2FB/']

    input_directories = map(os.path.expanduser, input_directories)
    plot_self_similarity_over_time(input_directories, 'servers')
    plot_zero_pages_over_time(input_directories)

    laptop_directories=['~/Dropbox/phd/veecycle/data/001B6333F86A/',
                        '~/Dropbox/phd/veecycle/data/001B6333F90A/',
                        '~/Dropbox/phd/veecycle/data/001B6334DE9F/']

    laptop_directories = map(os.path.expanduser, laptop_directories)
    plot_self_similarity_over_time(laptop_directories, 'laptops')
