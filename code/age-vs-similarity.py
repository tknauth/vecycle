#!/usr/bin/env python2.7

import argparse
import subprocess as sp
from datetime import datetime, timedelta
import glob
import itertools
import os.path
import multiprocessing as mp
import collections
import sys
import re
import gzip
import time
import Queue

# Assumes input files are in the current directory.

def get_datetime_from_filename(fn) :
    # Those files are from the MemoryBuddies paper
    if fn.startswith('memtracer-report-') :
        return datetime.strptime(fn, 'memtracer-report-%Y-%m-%d-%H-%M-%S.dat.hashes')
    else :
        pattern = re.compile('timestamp=(\d+)')
        match = pattern.search(fn)
        if None == match :
            print fn
            assert False
        timestamp = int(match.group(1))
        return datetime.fromtimestamp(timestamp)

# files = glob.glob('memtracer-report-2008-09-07-*.dat.hashes')
# files = sorted(files)

# tracereader_exec_path='/mnt/myvg-data/home/thomas/Downloads/traceReader'
# for f in *.dat ; do ~/Downloads/traceReader $f > $f.hashes ; done

class MemorySnapshotStatistics :
    def __init__(self) :
        self.file1 = None
        self.file2 = None
        self.s1_nr_non_zero_pages = 0
        self.s2_nr_non_zero_pages = 0
        self.s1_nr_unique_pages = 0
        self.s2_nr_unique_pages = 0
        self.matching_hashes = 0
        self.shifted_forward = 0
        self.shifted_back = 0
        self.stationary = 0
        self.total_pages = 0

    def __str__(self) :
        s = ''
        s += 'f1=%s '%(self.file1)
        s += 'f2=%s '%(self.file2)
        datetime1 = get_datetime_from_filename(self.file1)
        datetime2 = get_datetime_from_filename(self.file2)
        assert datetime1 < datetime2
        s += 'timedelta_sec=%d '%(datetime2-datetime1).total_seconds()
        s += 'total_pages=%d '%(self.total_pages)

        s += 'f1_nr_unique_pages=%d '%(self.s1_nr_unique_pages)
        s += 'f2_nr_unique_pages=%d '%(self.s2_nr_unique_pages)

        s += 'f1_nr_nonzero_pages=%d '%(self.s1_nr_non_zero_pages)
        s += 'f2_nr_nonzero_pages=%d '%(self.s2_nr_non_zero_pages)

        s += 'matching_hashes=%d '%(self.matching_hashes)

        s += 'shifted_forward=%d '%(self.shifted_forward)
        s += 'shifted_back=%d '%(self.shifted_back)
        s += 'stationary=%d '%(self.stationary)
        s += 'reusable_pages=%d'%(self.reusable_pages())

        return s

    def reusable_pages(self) :
        return self.shifted_forward + self.shifted_back + self.stationary

def open_trace_file(fn) :
    if fn.endswith('.gz') : return gzip.open(fn)
    else : return open(fn)

superfasthash_zeros_hash = int('b5e723dc', 16)

class Fingerprint :
    def __init__(self, path) :
        self.path = path
        self.total_pages = 0
        self.hash2pages = collections.defaultdict(list)
        self.calculate_stats()

    def calculate_stats(self) :
        f = open_trace_file(self.path)
        for line in f :
            hash = int(line, 16)
            self.hash2pages[hash].append(self.total_pages)
            self.total_pages += 1

    def nr_zero_pages(self) :
        return len(self.hash2pages[superfasthash_zeros_hash])

    def nr_non_zero_pages(self) :
        return self.total_pages - self.nr_zero_pages()

    def compare_with(self, old) :
        self.forward = self.back = self.stationary = self.matching_hashes = 0
        self.reusable_pages = 0

        for hash in self.hash2pages :

            # Ignore zero pages for similarity and shift analysis.
            if hash == superfasthash_zeros_hash : continue

            if not hash in old.hash2pages : continue

            old_pages = old.hash2pages[hash]
            max_old_page = max(old_pages)

            self.matching_hashes += 1
            self.reusable_pages += len(self.hash2pages[hash])

            for page in self.hash2pages[hash] :
                if page in old_pages : self.stationary += 1
                elif page > max_old_page : self.back += 1
                elif page < max_old_page : self.forward += 1

    def nr_unique_pages(self) :
        return len(self.hash2pages)

def determine_similarity(a, b) :

    res = MemorySnapshotStatistics()
    res.file1 = a
    res.file2 = b

    fp_old = Fingerprint(a)
    fp_new = Fingerprint(b)

    fp_new.compare_with(fp_old)

    res.s1_nr_unique_pages = len(fp_old.hash2pages)
    res.s2_nr_unique_pages = len(fp_new.hash2pages)
    res.s1_nr_non_zero_pages = fp_old.nr_non_zero_pages()
    res.s2_nr_non_zero_pages = fp_new.nr_non_zero_pages()

    res.matching_hashes = fp_new.matching_hashes
    res.shifted_back = fp_new.back
    res.shifted_forward = fp_new.forward
    res.stationary = fp_new.stationary

    assert fp_new.back + fp_new.forward + fp_new.stationary == fp_new.reusable_pages

    assert fp_old.total_pages == fp_new.total_pages
    res.total_pages = fp_old.total_pages

    return res

def worker(work_queue, result_queue) :

    while True :
        try :
            (a, b) = work_queue.get()
            result = determine_similarity(a, b)
            result_queue.put(result)
            work_queue.task_done()
        except Queue.Empty :
            print >> sys.stderr, 'worker quitting'
            break

def parse_arguments():
    parser = argparse.ArgumentParser(description='Process fingerprints to create statistics from them.')
    parser.add_argument('--input', nargs='?', default='*',
                        help='restrict input files using a pattern')
    parser.add_argument('--distance', nargs='?', default=24 * 60 * 60, type=int,
                        help='The maximum time difference to consider (in seconds). Default is 24 hours.')
    args = parser.parse_args()
    return args

if __name__ == '__main__' :

    args = parse_arguments()
    files = glob.glob(args.input)
    files = sorted(files)

    work_queue = mp.JoinableQueue()
    total_work_items = 0
    for (a, b) in itertools.combinations(files, 2) :
        time_a = get_datetime_from_filename(a)
        time_b = get_datetime_from_filename(b)

        if time_a > time_b :
            time_a, time_b = time_b, time_a

        if (time_b - time_a).total_seconds() > args.distance :
            continue

        work_queue.put((a, b))
        total_work_items += 1

    nr_cpu_cores=mp.cpu_count()
    result_queue = mp.JoinableQueue()
    processes = []
    for i in range(nr_cpu_cores) :
        p = mp.Process(target=worker, args=(work_queue, result_queue))
        processes.append(p)
        p.start()

    done_work_items = 0
    while done_work_items < total_work_items :
        item = result_queue.get(True)
        print item
        done_work_items += 1
        if done_work_items % 100 == 0 :
            print >> sys.stderr, '%d/%d items done'%(done_work_items, total_work_items)

    for p in processes :
        p.terminate()
