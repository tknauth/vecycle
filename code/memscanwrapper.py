#!/usr/bin/env python2.7

import subprocess as sp
import time
import datetime
import shlex
import socket

def generate_output_filename() :
    hostname = socket.gethostname()
    timestamp = time.strftime('%Y-%m-%d-%H-%M-%S')

    stdout = sp.check_output('last -F reboot | head -n1', shell=True)
    time_string = ' '.join(stdout.split()[4:9])
    reboot_datetime = datetime.datetime.strptime(time_string ,'%a %b %d %H:%M:%S %Y')
    reboot_formatted_string = reboot_datetime.strftime('%Y-%m-%d-%H-%M-%S')
    return 'memscan_hostname=%s_timestamp=%s_lastreboot=%s'%(hostname, timestamp, reboot_formatted_string)

def main() :
    output_filename = generate_output_filename()
    print output_filename
    cmd = 'sudo /mnt/myvg-data/home/thomas/Dropbox/phd/veecycle/code/memscan | gzip - > /mnt/myvg-data/home/thomas/Dropbox/phd/veecycle/data/desktop/%s.gz'%(output_filename)
    sp.check_call(cmd, shell=True)

if __name__ == '__main__' :
    main()
