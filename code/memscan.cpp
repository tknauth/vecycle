#include <cstdint> /* Replace with <stdint.h> if appropriate */
#include <cstdlib>
#include <iomanip>

/* Code originally from: */
/* http://www.azillionmonkeys.com/qed/hash.html */

#undef get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__) \
    || defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (get16bits)
#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8)\
                      +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif

uint32_t SuperFastHash (const char * data, int len) {
    uint32_t hash = len, tmp;
    int rem;

    if (len <= 0 || data == NULL) return 0;

    rem = len & 3;
    len >>= 2;

    /* Main loop */
    for (;len > 0; len--) {
        hash  += get16bits (data);
        tmp    = (get16bits (data+2) << 11) ^ hash;
        hash   = (hash << 16) ^ tmp;
        data  += 2*sizeof (uint16_t);
        hash  += hash >> 11;
    }

    /* Handle end cases */
    switch (rem) {
    case 3: hash += get16bits (data);
        hash ^= hash << 16;
        hash ^= ((signed char)data[sizeof (uint16_t)]) << 18;
        hash += hash >> 11;
        break;
    case 2: hash += get16bits (data);
        hash ^= hash << 11;
        hash += hash >> 17;
        break;
    case 1: hash += (signed char)*data;
        hash ^= hash << 10;
        hash += hash >> 1;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
}

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <cassert>

using namespace std;

struct command_line_parameters {
    command_line_parameters(void) : input_file_name("/dev/mem") {}

    std::string input_file_name;
};

void handle_command_line_arguments(int argc, char* argv[], command_line_parameters& p) {
    if (argc >= 2) {
        p.input_file_name = string(argv[1]);
    }
}

void dev_mem_ranges(std::list<std::pair<uint64_t, uint64_t> >& ranges) {

    std::string line;
    std::ifstream infile("/proc/iomem");
    while (std::getline(infile, line)) {
        /* cout << line << endl; */
        if (line.find("System RAM") == string::npos) continue;
        std::vector<std::string> strs;
        boost::split(strs, line, boost::is_any_of(":"));
        string range = strs[0];
        boost::split(strs, range, boost::is_any_of("-"));
        stringstream ss;
        ss << std::hex << strs[0];
        uint64_t begin = 0;
        ss >> begin;
        ss.clear();
        ss << std::hex << strs[1];
        uint64_t end = 0;
        ss >> end;

        ranges.push_back(std::pair<uint64_t, uint64_t>(begin, end));
    }
}

void determine_ranges(const command_line_parameters& p,
                      std::list<std::pair<uint64_t, uint64_t> >& ranges) {

    if (p.input_file_name == string("/dev/mem")) {
        dev_mem_ranges(ranges);
    }
}

void calc_and_output_hash(const char* buf, uint64_t len) {
    uint32_t hash = SuperFastHash(buf, len);
    cout << std::setfill('0') << std::setw(8) << std::hex << hash << endl;
}

int main(int argc, char* argv[]) {

    command_line_parameters p;
    handle_command_line_arguments(argc, argv, p);
    cerr << p.input_file_name << endl;

    std::list<std::pair<uint64_t, uint64_t> > ranges;
    determine_ranges(p, ranges);

    std::istream *is;
    if (p.input_file_name == "-") is = &std::cin;
    else is = new std::ifstream(p.input_file_name, std::ios::binary);
    assert (*is);

    const uint64_t page_size = 4096;
    char buf[page_size];

    if (ranges.size() > 0) {
        for (std::list<pair<uint64_t, uint64_t> >::iterator it = ranges.begin();
             it != ranges.end(); it++) {

            const uint64_t start = it->first;
            const uint64_t end   = it->second;

            uint64_t cur = start;
            is->seekg(start);
            do {
                uint64_t len = min(page_size, end-cur+1);
                assert(len > 0);
                is->read(buf, len);
                // uint32_t hash = SuperFastHash(buf, len);
                // cout << std::setfill('0') << std::setw(8) << std::hex << hash << endl;
                calc_and_output_hash(buf, len);
                cur += len;
            } while (cur < end && *is);
        }
    } else {
        while (*is) {
            is->read(buf, page_size);
            if (is->gcount() == 0) continue;

            assert(is->gcount() == page_size);
            calc_and_output_hash(buf, is->gcount());
        };
    }

    if (p.input_file_name != "-") { delete is; is = NULL; }

    return 0;
}
