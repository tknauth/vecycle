#!/usr/bin/env python2.7

import math
import argparse
import matplotlib
matplotlib.use('pdf')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'Times'
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename, max_time_delta_sec) :
    xys = []
    with open(filename) as f :
        for line in f.readlines() :
            line = line.strip()
            records = line.split()
            d = {}
            print records
            for (k, v) in map(lambda s : s.split('=', 1), records) :
                d[k] = v

            time_delta_seconds = int(d['timedelta_sec'])

            if time_delta_seconds > max_time_delta_sec : continue

            similarity = int(d['matching_hashes']) / float(d['f2_nr_unique_pages'])
            xy = (time_delta_seconds, similarity)
            xys.append(xy)
    return xys

def parse_arguments() :
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('--data')
    # 2.2 inch for 3 figures side-by-side spanning two columns
    # 6.2 inch for single two-column figure
    parser.add_argument('--width', type=float, default=2.3)
    parser.add_argument('--height', type=float, default=2.3/1.3)

    parser.add_argument('--title', nargs='?')
    parser.add_argument('--annotate', action="store_true")
    parser.add_argument('--days', type=int, default=1)

    args = parser.parse_args()
    return args

def annotate(plt) :
    plt.text(23.5, 0.1, 'minimum', ha='right')
    plt.text(23.5, 0.78, 'maximum', ha='right')
    plt.text(23.5, 0.32, 'average', ha='right')

if __name__ == '__main__' :

    args = parse_arguments()

    pylab.rc("figure.subplot", left=(30/72.27)/args.width)
    pylab.rc("figure.subplot", right=(args.width-10/72.27)/args.width)
    pylab.rc("figure.subplot", bottom=(25/72.27)/args.height)
    pylab.rc("figure.subplot", top=(args.height-20/72.27)/args.height)

    print args.width, args.height
    fig = plt.figure(figsize=(args.width, args.height))
    ax1 = fig.add_subplot(111)

    max_time_delta_sec = args.days * 24 * 60 * 60
    print max_time_delta_sec
    xys = parse_input(args.data, max_time_delta_sec)

    # print xys
    xs = []
    ymin = []
    ymax = []
    yavg = []

    color = {'min' : "#332288",
             'avg' : "#88CCEE",
             'max' : "#999933"}

    for upper_bound in range(45*60, max_time_delta_sec, 30*60) :
        lower_bound = upper_bound - 30*60
        bucket = filter(lambda (x,y) : x >= lower_bound and x < upper_bound, xys)
        if len(bucket) == 0 : continue
        # print lower_bound, upper_bound, len(bucket)
        xs.append(upper_bound)
        # print lower_bound, upper_bound, bucket
        ymin.append(min([y for (x, y) in bucket]))
        ymax.append(max([y for (x, y) in bucket]))
        yavg.append(np.average([y for (x, y) in bucket]))

    xs = [x/float(60*60) for x in xs]

    # percentiles = [25.0, 50.0, 90.0, 95.0, 99.0]
    # print percentiles, np.percentile(ys, percentiles)
    plt.plot(xs, ymin, linestyle='-', color=color['min'], label='ymin', linewidth=1.0)
    plt.plot(xs, ymax, linestyle='-', color=color['max'], label='ymax', linewidth=1.0)
    plt.plot(xs, yavg, linestyle='-', color=color['avg'], label='yavg', linewidth=1.0)

    # ax1.yaxis.grid(color='grey', linestyle='--', linewidth=0.5)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    # ax1.set_xticks([1,2,4,8,16,32])
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(0, 1)
    ax1.set_xlim(0)
    # ax1.set_yscale('log')
    if args.title :
        ax1.set_title(args.title, fontsize=fontsize)

    plt.xlabel('Time between snapshots [hours]')
    plt.ylabel('Snapshot similarity [0.0-1.0]')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # plt.legend(loc=2, numpoints=1, ncol=1)
    # leg = plt.gca().get_legend()
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    if args.annotate :
        annotate(plt)

    input_filename=os.path.basename(args.data)[0:-4]
    plot_dimensions='%din-%din'%(args.width, args.height)
    plt.savefig('%s-%s'%(input_filename, plot_dimensions))
