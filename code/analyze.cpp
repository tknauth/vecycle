#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cassert>
#include <map>
#include <set>

using namespace std;

struct Results {

    uint64_t shifted_front;
    uint64_t shifted_back;
    uint64_t identity;
    uint64_t new_hash;

    Results() {
        shifted_front = shifted_back = identity = new_hash = 0;
    }
    friend std::ostream& operator<< (std::ostream& s, const Results& r);
};

std::ostream& operator<< (std::ostream& s, const Results& r) {
    uint64_t sum = r.shifted_front + r.shifted_back + r.identity + r.new_hash;

    s << fixed;
    s << "shifted_front=" << r.shifted_front << ";" << setprecision(3) << r.shifted_front/float(sum) << " ";
    s << "shifted_back=" << r.shifted_back << ";" << setprecision(3) << r.shifted_back/float(sum) << " ";
    s << "identity=" << r.identity << ";" << setprecision(3) << r.identity/float(sum) << " ";
    s << "new_hash=" << r.new_hash << ";" << setprecision(3) << r.new_hash/float(sum);
    return s;
}


int main(int argc, char* argv[]) {

    assert(argc == 3);

    string line;
    ifstream infile(argv[1]);
    map<string, set<uint64_t> > hashes;

    uint64_t offset = 0;
    while (getline(infile, line)) {
        hashes[line].insert(offset);
        offset++;
    }

    cout << "hashes=" << offset << " ";
    cout << "unique_hashes=" << hashes.size() << " ";

    Results r;
    infile.close();
    infile.open(argv[2]);
    offset = 0;
    while (getline(infile, line)) {
        if (hashes.find(line) != hashes.end()) {
            if (hashes[line].find(offset) != hashes[line].end()) r.identity++;
            else if (*(max_element(hashes[line].begin(), hashes[line].end())) < offset) r.shifted_front++;
            else r.shifted_back++;
        } else {
            r.new_hash++;
        }
        offset++;
    }

    cout << r << endl;
    
    return 0;
}
