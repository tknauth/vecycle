#!/usr/bin/env python2.7

import sys
import ConfigParser
import os
import time
import subprocess as sp
import shlex
import socket
import operator
import argparse
import os.path

# Assumptions:
# * instances are started with virsh

kvm_binaries = ['kvm', 'qemu-system-x86', 'qemu-system-x86_64']

fifo_name = 'memscan'
fifo_path = '/tmp'

config = ConfigParser.ConfigParser()

def absolute_exec_path(executable):
    return os.path.join(config.get('global', 'code_dir'),
                        executable)


def build_filename(outputdir, timestamp, pid, vmname, uuid):
    hostname = socket.gethostname()
    filename = 'memscan-timestamp=%s' % (timestamp) + \
        '-hostname=%s-pid=%s-vmname=%s-uuid=%s' % (hostname, pid, vmname, uuid)

    return os.path.join(outputdir, filename)


def find_pc_ram_virtual_address_range(pid):
    pid = '12910'
    with open('/proc/%s/smaps' % (pid)) as f:
        line_count = 0
        for line in f:
            if line_count % 12 != 0:
                line_count += 1
                continue
            line = line.strip()
            address_range = line.split()[0]
            range_begin, range_end = address_range.split('-')
            range_begin, range_end = int(range_begin, 16), int(range_end, 16)

            if range_end - range_begin > 128 * 1024 * 1024:
                print hex(range_begin), hex(range_end)
                return range_begin, range_end

            line_count += 1


def dump_single_vm(pid, cmdline, timestamp, outputdir):

    print >> sys.stderr, 'fingerprinting qemu/kvm process with pid %s' % (pid)

    idx = cmdline.index('-name') # Throws ValueError if not in list
    assert len(cmdline) >= idx + 1 + 1
    vmname = cmdline[idx + 1]

    umask = os.umask(0)
    os.mkfifo('%s/%s' % (fifo_path, fifo_name))
    os.umask(umask)

    devnull = open('/dev/null', 'w')

    cmd_sudo_process = ['sudo', 'python2.7', absolute_exec_path('kvm-dump.py'),
                        pid]
    sudo_process = sp.Popen(cmd_sudo_process, stdout=sp.PIPE)

    hashes_filename = build_filename(outputdir,
                                     timestamp,
                                     pid,
                                     vmname,
                                     cmdline[cmdline.index('-uuid') + 1])

    # The memscan binary runs synchronously with the main Python thread
    cmd = '%s - >> %s' % (absolute_exec_path('memscan'), hashes_filename)
    # Pipes and redirections require shell=True
    sp.check_call(cmd, shell=True, stdout=devnull, stderr=devnull,
                  stdin=sudo_process.stdout)

    devnull.close()
    os.remove('%s/%s' % (fifo_path, fifo_name))
    sp.check_call(shlex.split('gzip %s' % (hashes_filename)))


def is_virsh_qemu_process(fields):

    if not (reduce(operator.or_,
                   [fields[0].endswith(x) for x in kvm_binaries])):
        return False
    if 0 == fields.count('-name'):
        return False
    if 0 == fields.count('-m'):
        return False
    if 0 == fields.count('-uuid'):
        return False

    return True


# @return True, if uuid or name matches the VM. False, otherwise.
def filter_uuid_or_name(args, fields):
    if args.vmname:
        if '-name' in fields:
            idx = fields.index('-name')
            if fields[idx + 1] in args.vmname:
                return True

    if args.uuid:
        if '-uuid' in fields:
            idx = fields.index('-uuid')
            if fields[idx + 1] in args.uuid:
                return True

    return False


def main(args):

    pids = set()
    for name in kvm_binaries:
        try:
            stdout = sp.check_output(shlex.split('pgrep -f ' + name))
            for pid in stdout.split():
                pids.add(pid)
        except sp.CalledProcessError, e:
            if e.returncode != 1:
                raise e

    timestamp = int(time.time())

    for pid in pids:
        with open('/proc/%s/cmdline' % (pid)) as f:
            cmdline = f.readline()
            qemu_proc_cmdline = cmdline.split('\0')
            if not is_virsh_qemu_process(qemu_proc_cmdline):
                continue

            if not filter_uuid_or_name(args, qemu_proc_cmdline):
                continue

            try:
                dump_single_vm(pid, qemu_proc_cmdline, timestamp,
                               config.get('global', 'output_dir'))
            except sp.CalledProcessError, e:
                print >> sys.stderr, \
                    'fingerprinting VM with pid %s failed with error %s' % (pid, str(e))


def parse_arguments():
    parser = argparse.ArgumentParser(description='Calculate memory fingerprint for QEMU virtual machines.')
    parser.add_argument('--vmname', nargs='+',
                        help='only fingerprint VMs with these names')
    parser.add_argument('--uuid', nargs='+',
                        help='only fingerprint VMs with these UUIDs')
    parser.add_argument('--conffile',
                        help='Configuration file')
    args = parser.parse_args()
    return args

if __name__ == '__main__':

    args = parse_arguments()

    config.readfp(open(args.conffile))

    main(args)
